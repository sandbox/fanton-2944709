<?php

namespace Drupal\basic_site\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Paso de instalacion para pedir datos basicos sobre el sitio web
 */
class DatosBasicSiteForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'install_datos_basic_site_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['#title'] = $this->t('Datos del sitio');

    $form['dominio'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Dominio principal'),
      '#maxlength' => 255,
      '#required' => TRUE,
      '#description' => $this->t('Ej: grupoite.com.uy'),
    ];

    $form['nombre'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Nombre del sitio'),
      '#maxlength' => 255,
      '#required' => TRUE,
      '#description' => $this->t('Ej: Grupo ITe'),
    ];

    $form['slogan'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Slogan'),
      '#maxlength' => 255,
      '#description' => $this->t('Ej: Desarrollo Web'),
    ];

    $form['direccion'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Dirección'),
      '#maxlength' => 255,
      '#description' => $this->t('Ej: Agraciada 448, Salto, Uruguay'),
    ];

    $form['paginas'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Títulos de páginas básicas'),
      '#description' => $this->t('Uno por línea'),
    ];

    $form['logo'] = [
      '#type' => 'file',
      '#title' => $this->t('Logo'),
    ];

    $form['ruta_logo'] = [
      '#type' => 'hidden',
      '#default_value' => drupal_get_path('theme', 'grupoite'),
    ];

    $form['idiomas'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Múltiples idiomas'),
    ];

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save and continue'),
      '#weight' => 15,
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  /*public function validateForm(array &$form, FormStateInterface $form_state) {
    /*$file_upload = $this->getRequest()->files->get('files[logo]', NULL, TRUE);

    if ($file_upload && $file_upload->isValid()) {
      $form_state->setValue('logo', $file_upload->getRealPath());
    }

    $logo = file_save_upload('logo');

  }*/

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    global $install_state;

    $campos = array(
      'dominio',
      'nombre',
      'slogan',
      'direccion',
      'paginas',
      'logo',
      'ruta_logo',
      'idiomas',
    );

    foreach ($campos as $key) {
      $install_state['datos_basic_site'][$key] = $form_state->getValue($key);
    }

    /*
    if ($path = $form_state->getValue('logo')) {
      try {
        if ($data = file_get_contents($path))
          echo "data";
        echo $path;
        if ($file = file_unmanaged_save_data($data, 'temporary://', FILE_EXISTS_REPLACE)) {
          echo "bien";
        }
        else {
          echo "mal";
        }
      }
      catch (\Exception $e) {
        drupal_set_message($this->t('No se pudo guardar el logo. El mensaje de error es: <em>@message</em>', ['@message' => $e->getMessage()]), 'error');
      }
    }
    else {
      echo "sin logo";;
    }*/

  }

}
