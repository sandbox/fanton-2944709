<?php
/**
 * @file
 * Enables modules and site configuration for a standard site installation.
 */

/**
 * Need to do a manual include since this install profile never actually gets
 * installed so therefore its code cannot be autoloaded.
 */
/*use Drupal\Core\Config\StorageComparer;
use Drupal\Core\Config\ConfigImporter;
use Drupal\Core\Config\ConfigImporterException;
use Drupal\Core\Site\Settings;*/
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\Node;
use Drupal\menu_link_content\Entity\MenuLinkContent;
use Drupal\contact\Entity\ContactForm;

include_once __DIR__ . '/src/Form/DatosBasicSiteForm.php';

function basic_site_preprocess_install_page(&$variables) {
  // Seven has custom styling for the install page. Adjuntamos estilo propio
  $variables['#attached']['library'][] = 'grupoite/install-page';
}

function basic_site_install_tasks_alter(&$tasks, $install_state) {
  // Ocultamos paso de seleccion de idioma, lo definimos en el .info del perfil
  $tasks['install_select_language']['display'] = FALSE;
  /*
    $task_map = array(
      'install_select_language',
      'install_download_translation',
      'install_select_profile',
      'install_load_profile',
      'install_verify_requirements',
      'install_settings_form',
      'install_write_profile',
      'install_verify_database_ready',
      'install_base_system',
      'install_datos_basic_site',
      // All tasks below are executed in a regular, full Drupal environment.
      'install_bootstrap_full',
      'install_profile_modules',
      'install_profile_themes',
      'install_install_profile',
      'install_import_translations',
      'install_configure_form',
    );

    $new_tasks = array();

    foreach ($task_map as $task) {
      $new_tasks[$task] = $tasks[$task];
      unset($tasks[$task]);
    }

    $new_tasks = $new_tasks + $tasks;

    $tasks = $new_tasks;
  */

  // Ver http://www.coalmarch.com/blog/things-ive-learned-while-creating-an-installation-profile-for-drupal-7
  // Muevo la tarea de pedir datos de sitio, antes de configurar la base de datos
  /*$key = array_search('install_settings_form', array_keys($tasks));
  $array_datos_basic_site = $tasks['install_datos_basic_site'];
  unset($tasks['install_datos_basic_site']);
  $tasks = array_slice($tasks, 0, $key, true) +
           array('install_datos_basic_site' => $array_datos_basic_site) +
           array_slice($tasks, $key, NULL, true);*/
}

/*function basic_site_install_tasks($install_state) {
  $tasks = array(
    'install_datos_basic_site' => array(
      'display_name' => t('Datos del sitio'),
      'type' => 'form',
      'function' => 'Drupal\basic_site\Form\DatosBasicSiteForm',
    ),
  );

  return $tasks;
}*/

/*function basic_site_install_tasks($install_state) {
  $tasks = array(
    'install_datos_basic_site' => array(
      'display_name' => t('Datos del sitio'),
      'type' => 'form',
      'function' => 'Drupal\basic_site\Form\DatosBasicSiteForm',
    ),
  );

  return $tasks;
}*/

/**
 * Implements hook_form_FORM_ID_alter() for install_configure_form().
 *
 * Allows the profile to alter the site settings form.
 */
function basic_site_form_install_settings_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  /*global $install_state;
  $form['settings']['mysql']['database']['#default_value'] = substr($install_state['datos_basic_site']['dominio'], 0, 8) . '_drupal8';*/

  $form['settings']['mysql']['database']['#title']         = $form['settings']['mysql']['database']['#title'] . ' (tipo cpanel)';
  $form['settings']['mysql']['database']['#default_value'] = '_drupal8';
  $form['settings']['mysql']['database']['#description']   = t('Utilice como prefijo los 8 primeros caracteres del dominio');

  $form['settings']['mysql']['username']['#default_value'] = 'root';

}

/**
 * Implements hook_form_FORM_ID_alter() for install_configure_form().
 *
 * Allows the profile to alter the site configuration form.
 */
function basic_site_form_install_configure_form_alter(&$form, FormStateInterface $form_state, $form_id) {

  $form['site_information']['site_name']['#description'] = t('Ej: Grupo ITe');

  $form['site_information']['basic_site_slogan'] = array(
    '#type'        => 'textfield',
    '#title'       => t('Slogan'),
    '#description' => t('Ej: Desarrollo Web'),
    '#weight'      => -19,
  );

  $form['site_information']['site_mail']['#default_value'] = 'info@grupoite.com.uy';

  $form['site_information']['basic_site_direccion'] = [
    '#type'        => 'textfield',
    '#title'       => t('Dirección'),
    '#maxlength'   => 255,
    '#description' => t('Ej: Agraciada 448, Salto, Uruguay'),
  ];

  $form['site_information']['basic_site_paginas'] = [
    '#type'          => 'textarea',
    '#title'         => t('Títulos de páginas básicas a crear'),
    '#default_value' => 'La empresa
Servicios',
    '#description'   => t('Una por línea'),
  ];

  $form['site_information']['basic_site_logo'] = [
    '#type'  => 'file',
    '#title' => t('Logo'),
  ];

  $form['site_information']['basic_site_ruta_logo'] = [
    '#type'          => 'hidden',
    '#default_value' => drupal_get_path('theme', 'grupoite'),
  ];

  $form['site_information']['basic_site_idiomas'] = [
    '#type'  => 'checkbox',
    '#title' => t('Múltiples idiomas'),
  ];

  // Account information defaults
  $form['admin_account']['account']['name']['#default_value'] = 'admin';
  $form['admin_account']['account']['mail']['#default_value'] = 'info@grupoite.com.uy';

  // Date/time settings
  $form['regional_settings']['site_default_country']['#default_value']  = 'UY';
  $form['regional_settings']['date_default_timezone']['#default_value'] = 'America/Montevideo';

  $form['update_notifications']['update_status_module'][2]['#default_value'] = 0;

  $form['#submit'][] = 'basic_site_form_install_configure_submit';

}

/**
 * Clean alias.
 *
 * @param string $text
 *
 * @return string
 *   Return machine name for text.
 */
function _basic_site_clean_alias($text) {
  return preg_replace('/\-+/', '-', strtolower(preg_replace('/[^a-zA-Z0-9_-]+/', '', str_replace(' ', '-', $text))));
}

function basic_site_form_install_configure_submit($form, FormStateInterface $form_state) {
  // Submission handler to sync the contact.form.feedback recipient.
  $site_mail = $form_state->getValue('site_mail');
  ContactForm::load('feedback')
    ->setRecipients([$site_mail])
    ->trustData()
    ->save();

  // Guardar el nombre y slogan del sitio
  // Si el perfil de instalacion esta en un idioma diferente a ingles, coloca "Drupal" como nombre, ver issue: https://www.drupal.org/node/2791405
  $site_name = $form_state->getValue('site_name');
  \Drupal::configFactory()
         ->getEditable('system.site')
         ->set('name', $site_name)
         ->save(TRUE);
  $site_slogan = $form_state->getValue('basic_site_slogan');
  \Drupal::configFactory()
         ->getEditable('system.site')
         ->set('slogan', $site_slogan)
         ->save(TRUE);

  $paginas = preg_split('/[\r\n]+/', $form_state->getValue('basic_site_paginas'), -1, PREG_SPLIT_NO_EMPTY);

  $id_nodo = 1;

  foreach ($paginas as $pagina) {
    $node = Node::create(array(
      'type'   => 'page',
      'title'  => $pagina,
      'uid'    => '1',
      'status' => 1,
      'path'   => '/' . _basic_site_clean_alias($pagina),
    ));
    $node->save();

    // Create link.
    MenuLinkContent::create([
      'title'     => $pagina,
      'link'      => ['uri' => 'internal:/node/' . $id_nodo],
      'menu_name' => 'main',
    ])->save();

    $id_nodo++;
  }
}

